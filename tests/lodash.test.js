const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

// my added test
test("Should computes the minimum value of array. If array is empty or falsey, undefined is returned.", () => {
    const array = [4, 2, 8, 6];
    let min_ele = _.min(array);
    expect(min_ele).to.eql(2);
})

test("Should computes the minimum value of array. If array is empty or falsey, undefined is returned.", () => {
    
    let min_ele=_.min([]);
    expect(min_ele).to.eql(undefined);
})

test("Shoud create an array of numbers (positive and/or negative) progressing from start up to, but not including, end. A step of -1 is used if a negative start is specified without an end or step. If end is not specified, it's set to start with start then set to 0.", () => {
    
    let array=_.range(4);
    expect(array).to.eql([0, 1, 2, 3]);
})
test("Shoud create an array of numbers (positive and/or negative) progressing from start up to, but not including, end. A step of -1 is used if a negative start is specified without an end or step. If end is not specified, it's set to start with start then set to 0.", () => {
    
    let array=_.range(-4);
    expect(array).to.eql([0, -1, -2, -3]);
})
test("Shoud create an array of numbers (positive and/or negative) progressing from start up to, but not including, end. A step of -1 is used if a negative start is specified without an end or step. If end is not specified, it's set to start with start then set to 0.", () => {
    
    let array=_.range(1, 5);
    expect(array).to.eql([1, 2, 3, 4]);
})
test("Shoud create an array of numbers (positive and/or negative) progressing from start up to, but not including, end. A step of -1 is used if a negative start is specified without an end or step. If end is not specified, it's set to start with start then set to 0.", () => {
    
    let array=_.range(0, 20, 5);
    expect(array).to.eql([0, 5, 10, 15]);
})
test("Shoud create an array of numbers (positive and/or negative) progressing from start up to, but not including, end. A step of -1 is used if a negative start is specified without an end or step. If end is not specified, it's set to start with start then set to 0.", () => {
    
    let array=_.range(0, -4, -1);
    expect(array).to.eql([0, -1, -2, -3]);
})
test("Shoud create an array of numbers (positive and/or negative) progressing from start up to, but not including, end. A step of -1 is used if a negative start is specified without an end or step. If end is not specified, it's set to start with start then set to 0.", () => {
    
    let array=_.range(0);
    expect(array).to.eql([]);
})
test("Should Creates an array of elements split into groups the length of size. If array can't be split evenly, the final chunk will be the remaining elements.", () => {
    
    let array=_.chunk(['a', 'b', 'c', 'd'], 3);
    expect(array).to.eql( [['a', 'b', 'c'], ['d']]);
})
test("Should Creates an array of elements split into groups the length of size. If array can't be split evenly, the final chunk will be the remaining elements.", () => {
    
    let array=_.chunk(['a', 'b', 'c', 'd'], 2);
    expect(array).to.eql([['a', 'b'], ['c', 'd']]);
})


