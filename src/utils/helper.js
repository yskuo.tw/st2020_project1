
let sorting = (array) => {
    return array.sort((a, b) => {
        if(a > b) return 1;
        else      return -1;
    });
}

let compare = (a, b) => {
    a=parseInt(a['PM2.5']);
    b=parseInt(b['PM2.5']);
    if (a>b)return 1;
    // else if (a==b) return 0;
    else return -1;
}

let average = (nums) => {
    console.log(nums);
    let sum = nums.reduce((previous, current) => current += previous);
    let avg = sum / nums.length;
    return Math.round(avg*100)/100 ;
}


module.exports = {
    sorting,
    compare,
    average
}